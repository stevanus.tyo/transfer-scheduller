/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author stevanustyo
 */
public class XMLParser {
    
    private static String xml_file = null;
  private static String xml_data = null;
  private static String err_message = null;
  private static DocumentBuilderFactory dbf = null;
  private static DocumentBuilder db = null;
  private static Document doc = null;
  private static File file = null;

  public static void ReadXMLFile() {

    try {

      file = new File(getXMLFile());

      dbf = DocumentBuilderFactory.newInstance();

      try {
        db = dbf.newDocumentBuilder();
      } catch (ParserConfigurationException Ex) {
        setErrXML(Ex.getMessage());
      }


      try {
        doc = db.parse(file);
      } catch (SAXException Ex) {
        setErrXML(Ex.getMessage());
      } catch (IOException Ex) {
        setErrXML(Ex.getMessage());
      }
      doc.getDocumentElement().normalize();

    } catch (Exception Ex) {
      setErrXML(Ex.getMessage());
    }

  }

  public static void setXMLFile(String xml_filex) {
    xml_file = xml_filex;
  }

  public static String getXMLFile() {
    return xml_file;
  }

  public static void setErrXML(String err_messagex) {
    err_message = err_messagex;
  }

  public static String getErrXML() {
    return err_message;
  }

  public static String XMLConfig(String config, String tag_name) {

   XMLParser.setXMLFile(LibConfig.file_config_xml);
    XMLParser.ReadXMLFile();
    String get_value = "";

    try {
      NodeList nodeLst = doc.getElementsByTagName(config);
      for (int s = 0; s < nodeLst.getLength(); s++) {
        Node fstNode = nodeLst.item(s);
        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
          Element fstElmnt = (Element) fstNode;
          NodeList fstNmElmntLst = fstElmnt.getElementsByTagName(tag_name);
          Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
          NodeList fstNm = fstNmElmnt.getChildNodes();
          get_value = ((Node) fstNm.item(0)).getNodeValue();
        }
      }
    } catch (Exception Ex) {
      setErrXML(Ex.getMessage());
    }
    return get_value;
  }

  public static void ReadXMLString() {

    try {
      dbf = DocumentBuilderFactory.newInstance();
      try {
        db = dbf.newDocumentBuilder();
      } catch (ParserConfigurationException Ex) {
        if (Ex.getMessage() != null) {
        }
        setErrXML(Ex.getMessage());
      }
      InputSource is = new InputSource();
      is.setCharacterStream(new StringReader(getXMLString()));

      try {
        doc = db.parse(is);
      } catch (SAXException Ex) {
 
        setErrXML(Ex.getMessage());
      } catch (IOException Ex) {
        setErrXML(Ex.getMessage());
      }
      doc.getDocumentElement().normalize();
    } catch (Exception Ex) {
      setErrXML(Ex.getMessage());
    }

  }

  public static void setXMLString(String _xml_data) {
    xml_data = _xml_data;
  }

  public static String getXMLString() {
    return xml_data;
  }

  public static String XMLString(String xml_data, String tag_root, String tag_name) {

    XMLParser.setXMLString(xml_data);
    XMLParser.ReadXMLString();
    String get_value = "";

    try {
      NodeList nodeLst = doc.getElementsByTagName(tag_root);
      for (int s = 0; s < nodeLst.getLength(); s++) {
        Node fstNode = nodeLst.item(s);
        if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
          Element fstElmnt = (Element) fstNode;
          NodeList fstNmElmntLst = fstElmnt.getElementsByTagName(tag_name);
          Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
          NodeList fstNm = fstNmElmnt.getChildNodes();
          if (((Node) fstNm.item(0)).getNodeValue() != null) {
            get_value = ((Node) fstNm.item(0)).getNodeValue();
          }
        }
      }
    } catch (Exception Ex) {
      get_value = "";
      setErrXML(Ex.getMessage());
    }

    if (get_value == null) {
      get_value = "";
    }
    return get_value.trim();
  }

  public static String[][] XMLStringParseByTag(String xml_data) {

    XMLParser.setXMLString(xml_data);
    XMLParser.ReadXMLString();
    String[][] get_value = null;
    NodeList nodeLst = null;

    try {
      nodeLst = doc.getElementsByTagName("*");
      get_value = new String[nodeLst.getLength()][2];
    } catch (Exception Ex) {
      setErrXML(Ex.getMessage());
    }
    int i = 0, x = 0, y = 0;

    try {
      for (i = 0; i < nodeLst.getLength(); i++) {
        Element element = (Element) nodeLst.item(i);
        if (element.getNodeType() == element.ELEMENT_NODE) {
          get_value[i][0] = element.getTagName();
        }
      }
    } catch (Exception Ex) {
      get_value[i][0] = "";
      setErrXML(Ex.getMessage());
    }


    nodeLst = doc.getElementsByTagName(get_value[0][0]);
    for (x = 0; x < nodeLst.getLength(); x++) {
      Node fstNode = nodeLst.item(x);
      if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
        for (y = 1; y < get_value.length; y++) {
          try {
            Element fstElmnt = (Element) fstNode;
            NodeList fstNmElmntLst = fstElmnt.getElementsByTagName(get_value[y][0]);
            Element fstNmElmnt = (Element) fstNmElmntLst.item(0);
            NodeList fstNm = fstNmElmnt.getChildNodes();
            get_value[y][1] = ((Node) fstNm.item(0)).getNodeValue();
          } catch (Exception Ex) {
            get_value[y][1] = "";
            setErrXML(Ex.getMessage());
          }
        }
      }
    }
    return get_value;
  }
    
}
