/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author stevanustyo
 */
public class LibFunction {
    
    private Connection mysql_db_conn;
    private boolean reconnect = true;
    private boolean result = false;
    private String err_message = "";
    private String query = "";
    
    public static String getDatetime(String format) {
    DateFormat dateFormat = null;
    Date date = null;
    try {
      dateFormat = new SimpleDateFormat(format.trim());
      date = new Date();
    } catch (Exception ex) {
      return "";
    }
    return dateFormat.format(date);
  }
    
    public Connection getMysqlConn() {
        if (this.mysql_db_conn == null) {
            try {
                DriverManager.setLoginTimeout(10000);
                this.mysql_db_conn = DriverManager.getConnection("jdbc:mysql://" + LibConfig.db_host
                        + ":" + LibConfig.db_port + "/" + LibConfig.db_name + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC",
                        LibConfig.db_user,
                        LibConfig.db_pass);
                setResult(true);
                setReconnect(false);
            } catch (SQLException Ex) {
                closeMysqlDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            } catch (Exception Ex) {
                closeMysqlDBConn();
                setReconnect(true);
                setResult(false);
                setErrMessage(Ex.getMessage());
            }
        }
        
        return this.mysql_db_conn;
    }
    
    private void setResult(boolean result) {
        this.result = result;
    }
    
    public void setReconnect(boolean reconnect) {
        this.reconnect = reconnect;
    }
    
    public void closeMysqlDBConn() {
        if (this.mysql_db_conn != null) {
            try {
                this.mysql_db_conn.close();
            } catch (SQLException Ex) {
            } catch (Exception Ex) {
            }
        }
    }
    
    public void setErrMessage(String err_message) {
        try {
            err_message = err_message.trim();
            if (!(err_message == null || err_message.isEmpty())) {
                this.err_message = err_message;
            }
        } catch (Exception Ex) {
            this.err_message = Ex.getMessage();
        }
    }
    
    public static String GetCurrentDirectory() {
    String result = "";
    File dir = null;
    try {
      dir = new File(".");
      result = dir.getCanonicalPath();
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
      return result = "";
    }
    return result.trim();
  }
    
    public void setQuery(String query) {
        this.query = query;
    }
    
    public String getQuery() {
        return this.query;
    }
    
}
