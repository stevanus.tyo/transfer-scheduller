/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

/**
 *
 * @author stevanustyo
 */
public class LibConfig {
    
    public static String db_connection_properties = "autoReconnect=false"
          + "&connectionCollation=latin1_general_ci"
          + "&useGmtMillisForDatetimes=false";
    public static String file_config_xml = LibFunction.GetCurrentDirectory() + "/appconfig.xml";
    
    public static String db_host = XMLParser.XMLConfig("config", "dbHost");
    public static String db_port = XMLParser.XMLConfig("config", "dbPort");
    public static String db_user = XMLParser.XMLConfig("config", "dbUsername");
    public static String db_pass = XMLParser.XMLConfig("config", "dbPassword");
    public static String db_name = XMLParser.XMLConfig("config", "dbName");
    
    public static String time_execute = XMLParser.XMLConfig("config", "timeExe");
    
    public static String url_end_point = XMLParser.XMLConfig("config", "urlEndPoint");
    public static String username_TWS = XMLParser.XMLConfig("config", "usernameTWS");
    public static String password_TWS = XMLParser.XMLConfig("config", "passwordTWS");
    public static String company_TWS = XMLParser.XMLConfig("config", "companyTWS");
    
}
