/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg_lib;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import pkg_main.mainapp;

/**
 *
 * @author stevanustyo
 */
public class FundTransfer {
    
    public static void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
        SOAPPart soapPart = soapMessage.getSOAPPart();

        String myNamespace = "t24";
        String myNamespaceURI = "T24WebServicesImpl";

        // SOAP Envelope
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);  

        // SOAP Body
        SOAPBody soapBody = envelope.getBody();
        
        SOAPElement soapBodyElem = soapBody.addChildElement("FundsTransfer", myNamespace);
        
        SOAPElement soapBodyElemWebReqCom = soapBodyElem.addChildElement("WebRequestCommon");
        SOAPElement soapBodyElemargWebReqCom1 = soapBodyElemWebReqCom.addChildElement("userName");
        SOAPElement soapBodyElemargWebReqCom2 = soapBodyElemWebReqCom.addChildElement("password");
        SOAPElement soapBodyElemargWebReqCom3 = soapBodyElemWebReqCom.addChildElement("company");        
        SOAPElement soapBodyElemOFS = soapBodyElem.addChildElement("OfsFunction");
        SOAPElement soapBodyElemargOFS1 = soapBodyElemOFS.addChildElement("messageId");        
        SOAPElement soapBodyElemFUNDSTRANSFERID = soapBodyElem.addChildElement("FUNDSTRANSFERIDIACCTTRFCMSType");
        SOAPElement soapBodyElemargFUNDSTRANSFERID1 = soapBodyElemFUNDSTRANSFERID.addChildElement("DebitAccount");
        SOAPElement soapBodyElemargFUNDSTRANSFERID2 = soapBodyElemFUNDSTRANSFERID.addChildElement("DebitAmount");
        SOAPElement soapBodyElemargFUNDSTRANSFERID3 = soapBodyElemFUNDSTRANSFERID.addChildElement("CreditAccount");
        SOAPElement soapBodyElemargFUNDSTRANSFERID4sub = soapBodyElemFUNDSTRANSFERID.addChildElement("gPAYMENTDETAILS");
        SOAPElement soapBodyElemargFUNDSTRANSFERID4sub1 = soapBodyElemargFUNDSTRANSFERID4sub.addChildElement("PaymentDetails");
        SOAPElement soapBodyElemargFUNDSTRANSFERID5 = soapBodyElemFUNDSTRANSFERID.addChildElement("KodeBiaya");
        SOAPElement soapBodyElemargFUNDSTRANSFERID6sub = soapBodyElemFUNDSTRANSFERID.addChildElement("gCOMMISSIONTYPE");
        SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1 = soapBodyElemargFUNDSTRANSFERID6sub.addChildElement("mCOMMISSIONTYPE");
        SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1ch1 = soapBodyElemargFUNDSTRANSFERID6sub1.addChildElement("JenisBiaya");
        SOAPElement soapBodyElemargFUNDSTRANSFERID6sub1ch2 = soapBodyElemargFUNDSTRANSFERID6sub1.addChildElement("NominalBiaya");
        SOAPElement soapBodyElemargFUNDSTRANSFERID7 = soapBodyElemFUNDSTRANSFERID.addChildElement("MSGID");
        
        mainapp isi = new mainapp();
        
        soapBodyElemargWebReqCom1.addTextNode(LibConfig.username_TWS); //username
        soapBodyElemargWebReqCom2.addTextNode(LibConfig.password_TWS); //password
        soapBodyElemargWebReqCom3.addTextNode(LibConfig.company_TWS); //company
        soapBodyElemargOFS1.addTextNode(isi.msgid); //msgid
        soapBodyElemargFUNDSTRANSFERID1.addTextNode(isi.accno); //accno
        soapBodyElemargFUNDSTRANSFERID2.addTextNode(isi.debitamt); //debitamt
        soapBodyElemargFUNDSTRANSFERID3.addTextNode(isi.creditacc); //creditaccno
        soapBodyElemargFUNDSTRANSFERID4sub1.addTextNode(isi.paymentdtl); //paymentdtl
        soapBodyElemargFUNDSTRANSFERID5.addTextNode(isi.kodebiaya); //kodebiaya
        soapBodyElemargFUNDSTRANSFERID6sub1ch1.addTextNode(isi.jenisbiaya); //jenisbiaya
        soapBodyElemargFUNDSTRANSFERID6sub1ch2.addTextNode(isi.nominalbiaya); //nominalbiaya
        soapBodyElemargFUNDSTRANSFERID7.addTextNode(isi.msgid); //msgid
        
    }

    public void callSoapWebService(String soapEndpointUrl, String soapAction) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapAction), soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            soapResponse.writeTo(System.out);
            System.out.println();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    public static SOAPMessage createSOAPRequest(String soapAction) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        createSoapEnvelope(soapMessage);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");

        return soapMessage;
    }
    
}
