/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author stevanustyo
 */

/**
 * @param args the command line arguments
 * @throws java.lang.InterruptedException
 * @throws java.sql.SQLException
 */

package pkg_main;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import pkg_lib.FundTransfer;
import pkg_lib.LibConfig;
import pkg_lib.LibFunction;

public class mainapp {
    
    public static String accno,
            creditacc,
            debitamt,
            paymentdtl,
            seqNumber,
            msgid,
            kodebiaya,
            jenisbiaya,
            nominalbiaya;
                 
    @SuppressWarnings("SleepWhileInLoop")
    public static void main(String[] args) throws InterruptedException, SQLException {
        
        String soapEndpointUrl = LibConfig.url_end_point;
        String soapAction = "";
        
        LibFunction mysqlq = new LibFunction();
        Statement stmt = null;
        ResultSet getResultSet = null;
        
        while(true){
            
            String dateFormat = LibFunction.getDatetime("dd");
            String timeNow = LibFunction.getDatetime("HH:mm:ss");
            
            // System.out.println(timeNow);

            if ( timeNow.equals("09:28:50") ) {

                stmt = mysqlq.getMysqlConn().createStatement();
                mysqlq.setQuery("SELECT * FROM T_TRF_SCHEDULER "
                                + "WHERE DATE_LOOP = " + "'"+ dateFormat +"'");
                getResultSet = stmt.executeQuery(mysqlq.getQuery());

                while(getResultSet.next()) {
                    
                    // Get Result
                    accno = getResultSet.getString("ACCOUNT_NO");
                    creditacc = getResultSet.getString("CREDIT_ACC_NO");
                    debitamt = getResultSet.getString("DEBIT_AMT");
                    paymentdtl = getResultSet.getString("PAYMENT_DETAIL");
                    kodebiaya = getResultSet.getString("KODE_BIAYA");
                    jenisbiaya = getResultSet.getString("JENIS_BIAYA");
                    nominalbiaya = getResultSet.getString("NOMINAL_BIAYA");
                    seqNumber = LibFunction.getDatetime("yyyyMMddHHmmssSSS");
                    msgid = accno + seqNumber;
                    
                    System.out.println("Result set : \n" + accno 
                            + " " + creditacc 
                            + " " + debitamt 
                            + " " + msgid + "\n");
                    
                    // Execute soap request
                    FundTransfer transfer = new FundTransfer();
                    transfer.callSoapWebService(soapEndpointUrl, soapAction);
                    
                }
            }
            
            Thread.sleep(1000);
        }

    }
    
}
